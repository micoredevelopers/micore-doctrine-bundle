<?php

$kernel = new \MiCore\DoctrineBundle\Tests\Fixtures\Kernel('test', true);
$kernel->boot();



$em = $kernel->getContainer()->get('doctrine')->getManager();

return new \Symfony\Component\Console\Helper\HelperSet(array(
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
));
