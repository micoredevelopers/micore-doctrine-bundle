<?php


namespace MiCore\DoctrineBundle\Tests;


use Doctrine\Common\Collections\Collection;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\FilterAnnotationsReader;
use MiCore\DoctrineBundle\Tests\Fixtures\Entity\Entity;
use MiCore\DoctrineBundle\Tests\Fixtures\Filter;
use MiCore\DoctrineBundle\Tests\Fixtures\EntityServiceRepository;
use MiCore\DoctrineBundle\Tests\Fixtures\FilterCollectionProperty;

class EntityServiceRepositoryTest extends KernelTestCase
{

    public function setUp(): void
    {
        self::bootKernel();
    }


    public function testCreateQueryBuilderByFilter()
    {

        $repository = $this->getRepository();

        $qb = $repository->createQueryBuilderByFilter('e', new Filter());

        $this->assertEquals($qb->getDql(), 'SELECT e FROM MiCore\DoctrineBundle\Tests\Fixtures\Entity\Entity e LEFT JOIN e.foo_t e_foo_t LEFT JOIN e_foo_t.foo_t2 e_foo_t_foo_t2 LEFT JOIN e.rel_table e_rel_table WHERE (((((((((((((((e.AndX1_Contains LIKE :AndX1_Contains AND e.AndX2_Contains LIKE :AndX2_Contains) AND (e_foo_t_foo_t2.DateComp_d_more = :foo_t_foo_t2_DateComp_d_more OR e_foo_t_foo_t2.DateComp_m_degfault = :foo_t_foo_t2_DateComp_m_degfault)) AND e_foo_t_foo_t2.DateComp_AndX_Y_less = :foo_t_foo_t2_DateComp_AndX_Y_less) AND (e.DateEq >= :DateEq AND e.DateEq <= :DateEq_6)) AND e.EndsWith LIKE :EndsWith) AND e.Eq = :Eq) AND e.Gt > :Gt) AND e.Gte >= :Gte) AND e.In IN(:In)) AND e.IsNull IS NULL) AND e.Lt < :Lt) AND e.Lte <= :Lte) AND rel_table.MemberOf MEMBER OF 33333) AND e.Neq <> :Neq) AND e.NotIn NOT IN(:NotIn)) AND e.StartsWith LIKE :StartsWith');

    }

    public function testCollectionPropertyInFilter()
    {
        $repository = $this->getRepository();
        $filter = new FilterCollectionProperty();

        $qb = $repository->createQueryBuilderByFilter('e', $filter);
        $this->assertEquals($qb->getDql(), 'SELECT e FROM MiCore\DoctrineBundle\Tests\Fixtures\Entity\Entity e');
        $this->assertInstanceOf(Collection::class, $filter->fooCollection());

        $filter->fooCollection()->add('val');
        $qb = $repository->createQueryBuilderByFilter('e', $filter);
        $this->assertEquals(1, $filter->fooCollection()->count());
        $this->assertEquals($qb->getDQL(), 'SELECT e FROM MiCore\DoctrineBundle\Tests\Fixtures\Entity\Entity e WHERE e.fooCollection IN(:fooCollection)');
    }

    public function testDbSaveElement()
    {

        $repository = self::$container->get(EntityServiceRepository::class);
        $repository->setFilterAnnotationReader(self::$container->get(FilterAnnotationsReader::class));

        $entity = new Entity();
        $repository->persist($entity);
        $repository->flush();

        $this->assertIsInt($entity->getId(), $entity->getId());

    }

    public function testReloadDb()
    {
        $repository = self::$container->get(EntityServiceRepository::class);
        $repository->setFilterAnnotationReader(self::$container->get(FilterAnnotationsReader::class));
        $elements = $repository->findAll();
        $this->assertEquals($elements, []);
    }
}
