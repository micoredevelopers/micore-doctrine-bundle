<?php


namespace MiCore\DoctrineBundle\Tests;


use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\FilterAnnotationsReader;
use MiCore\DoctrineBundle\Tests\Fixtures\EntityServiceRepository;

class KernelTestCase extends \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase
{

    public function setUp(): void
    {
        self::bootKernel();
    }

    protected function getRepository(): EntityServiceRepository
    {
        $repository = self::$container->get(EntityServiceRepository::class);
        $repository->setFilterAnnotationReader(self::$container->get(FilterAnnotationsReader::class));
        return $repository;
    }

}
