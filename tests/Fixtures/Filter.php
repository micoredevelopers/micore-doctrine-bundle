<?php


namespace MiCore\DoctrineBundle\Tests\Fixtures;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\AndX;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\Comp;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\Contains;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\DateComp;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\DateEq;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\EndsWith;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\Eq;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\Gt;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\Gte;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\In;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\IsNull;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\Lt;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\Lte;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\MemberOf;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\Neq;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\NotIn;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\StartsWith;

class Filter
{

    /**
     * @Contains("AndX1_Contains")
     * @AndX()
     * @Contains("AndX2_Contains")
     */
    private $foo = 'string';
    /**
     * @DateComp("foo_t.foo_t2.DateComp_d_more", accuracy="d", moreLess="more")
     * @DateComp("foo_t.foo_t2.DateComp_m_degfault", accuracy="m")
     * @AndX()
     * @DateComp("foo_t.foo_t2.DateComp_AndX_Y_less", accuracy="Y", moreLess="less")
     */
    private $foo1;
    /**
     * @DateEq("DateEq", accuracy="m")
     */
    private $foo2;
    /**
     * @EndsWith("EndsWith")
     */
    private $foo3 = 'string';
    /**
     * @Eq("Eq")
     */
    private $foo4 = 'string';
    /**
     * @Gt("Gt")
     */
    private $foo6 = 1;
    /**
     * @Gte("Gte")
     */
    private $foo7 = 2;
    /**
     * @In("In")
     */
    private $foo8 = [1,2];
    /**
     * @IsNull("IsNull")
     */
    private $foo10 = true;
    /**
     * @Lt("Lt")
     */
    private $foo11 = 10;
    /**
     * @Lte("Lte")
     */
    private $foo12 = 10;
    /**
     * @MemberOf("rel_table.MemberOf")
     */
    private $foo13 = '33333';
    /**
     * @Neq("Neq")
     */
    private $foo14 = 4;
    /**
     * @NotIn("NotIn")
     */
    private $foo15 = [1,2];
    /**
     * @StartsWith("StartsWith")
     */
    private $foo16 = 'string';

    /**
     * @In("fooCollection")
     * @var Collection
     */
    private $fooCollection;


    public function __construct()
    {
        $this->foo1 = new \DateTime();
        $this->foo2 = new \DateTime();
        $this->fooCollection = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getFoo()
    {
        return $this->foo;
    }

    /**
     * @param mixed $foo
     */
    public function setFoo($foo): void
    {
        $this->foo = $foo;
    }

    /**
     * @return mixed
     */
    public function getFoo1()
    {
        return $this->foo1;
    }

    /**
     * @param mixed $foo1
     */
    public function setFoo1($foo1): void
    {
        $this->foo1 = $foo1;
    }

    /**
     * @return mixed
     */
    public function getFoo2()
    {
        return $this->foo2;
    }

    /**
     * @param mixed $foo2
     */
    public function setFoo2($foo2): void
    {
        $this->foo2 = $foo2;
    }

    /**
     * @return mixed
     */
    public function getFoo3()
    {
        return $this->foo3;
    }

    /**
     * @param mixed $foo3
     */
    public function setFoo3($foo3): void
    {
        $this->foo3 = $foo3;
    }

    /**
     * @return mixed
     */
    public function getFoo4()
    {
        return $this->foo4;
    }

    /**
     * @param mixed $foo4
     */
    public function setFoo4($foo4): void
    {
        $this->foo4 = $foo4;
    }

    /**
     * @return mixed
     */
    public function getFoo6()
    {
        return $this->foo6;
    }

    /**
     * @param mixed $foo6
     */
    public function setFoo6($foo6): void
    {
        $this->foo6 = $foo6;
    }

    /**
     * @return mixed
     */
    public function getFoo7()
    {
        return $this->foo7;
    }

    /**
     * @param mixed $foo7
     */
    public function setFoo7($foo7): void
    {
        $this->foo7 = $foo7;
    }

    /**
     * @return mixed
     */
    public function getFoo8()
    {
        return $this->foo8;
    }

    /**
     * @param mixed $foo8
     */
    public function setFoo8($foo8): void
    {
        $this->foo8 = $foo8;
    }

    /**
     * @return mixed
     */
    public function getFoo10()
    {
        return $this->foo10;
    }

    /**
     * @param mixed $foo10
     */
    public function setFoo10($foo10): void
    {
        $this->foo10 = $foo10;
    }

    /**
     * @return mixed
     */
    public function getFoo11()
    {
        return $this->foo11;
    }

    /**
     * @param mixed $foo11
     */
    public function setFoo11($foo11): void
    {
        $this->foo11 = $foo11;
    }

    /**
     * @return mixed
     */
    public function getFoo12()
    {
        return $this->foo12;
    }

    /**
     * @param mixed $foo12
     */
    public function setFoo12($foo12): void
    {
        $this->foo12 = $foo12;
    }

    /**
     * @return mixed
     */
    public function getFoo13()
    {
        return $this->foo13;
    }

    /**
     * @param mixed $foo13
     */
    public function setFoo13($foo13): void
    {
        $this->foo13 = $foo13;
    }

    /**
     * @return mixed
     */
    public function getFoo14()
    {
        return $this->foo14;
    }

    /**
     * @param mixed $foo14
     */
    public function setFoo14($foo14): void
    {
        $this->foo14 = $foo14;
    }

    /**
     * @return mixed
     */
    public function getFoo15()
    {
        return $this->foo15;
    }

    /**
     * @param mixed $foo15
     */
    public function setFoo15($foo15): void
    {
        $this->foo15 = $foo15;
    }

    /**
     * @return mixed
     */
    public function getFoo16()
    {
        return $this->foo16;
    }

    /**
     * @param mixed $foo16
     */
    public function setFoo16($foo16): void
    {
        $this->foo16 = $foo16;
    }

}
