<?php


namespace MiCore\DoctrineBundle\Tests\Fixtures\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Entity
 * @package MiCore\DoctrineBundle\Tests\Fixtures\Entity
 *
 * @ORM\Entity()
 */
class Entity
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string|null
     */
    private $foo;

    public $mark;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getFoo(): ?string
    {
        return $this->foo;
    }

    /**
     * @param string|null $foo
     */
    public function setFoo(?string $foo): void
    {
        $this->foo = $foo;
    }

}
