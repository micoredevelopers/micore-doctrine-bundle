<?php


namespace MiCore\DoctrineBundle\Tests\Fixtures;


use DAMA\DoctrineTestBundle\DAMADoctrineTestBundle;
use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use MiCore\DoctrineBundle\MiCoreDoctrineBundle;
use MiCore\KernelTest\KernelTest;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

class Kernel extends KernelTest
{

    protected function bundles(): array
    {
        return [
            MiCoreDoctrineBundle::class,
            DoctrineBundle::class,
            DAMADoctrineTestBundle::class
        ];
    }

    public function configureContainer(ContainerBuilder $c)
    {
        $c->loadFromExtension('doctrine', [
            'dbal' => [
                'url' => 'sqlite:///%kernel.project_dir%/var/app.db'
            ],
            'orm' => [
                'mappings' => [
                    'App' => [
                        'is_bundle' => false,
                        'type' => 'annotation',
                        'dir' => '%kernel.project_dir%/tests/Fixtures/Entity',
                        'prefix' => 'MiCore\DoctrineBundle\Tests\Fixtures\Entity',
                        'alias' => 'App'
                    ]
                ]
            ]
        ]);

        $c->autowire(EntityServiceRepository::class)->setPublic(true);
        $c->autowire(EventListener::class);
    }
}
