<?php


namespace MiCore\DoctrineBundle\Tests\Fixtures;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\In;

class FilterCollectionProperty
{

    /**
     * @In("fooCollection")
     * @var Collection
     */
    private $fooCollection;


    public function __construct()
    {
        $this->fooCollection = new ArrayCollection();
    }

    /**
     * @return Collection
     */
    public function fooCollection(): Collection
    {
        return $this->fooCollection;
    }
}
