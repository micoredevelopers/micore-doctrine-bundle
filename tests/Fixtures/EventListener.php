<?php


namespace MiCore\DoctrineBundle\Tests\Fixtures;


use Doctrine\ORM\Event\LifecycleEventArgs;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPostLoadListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPostPersistListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPostRemoveListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPostUpdateListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPrePersistListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPreRemoveListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPreUpdateListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PostLoadListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PostPersistListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PostRemoveListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PostUpdateListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PrePersistListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PreRemoveListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PreUpdateListenerInterface;
use MiCore\DoctrineBundle\Tests\Fixtures\Entity\Entity;

class EventListener implements PrePersistListenerInterface,
    PreUpdateListenerInterface,
    PreRemoveListenerInterface,
    PostPersistListenerInterface,
    PostLoadListenerInterface,
    PostUpdateListenerInterface,
    PostRemoveListenerInterface,
    EntityPostLoadListenerInterface,
    EntityPreRemoveListenerInterface,
    EntityPreUpdateListenerInterface,
    EntityPrePersistListenerInterface,
    EntityPostPersistListenerInterface,
    EntityPostRemoveListenerInterface,
    EntityPostUpdateListenerInterface
{

    public static function entityClass(): string
    {
        return Entity::class;
    }

    public function entityPostLoad(object $entity, LifecycleEventArgs $eventArgs): void
    {
        $entity->mark[] = __FUNCTION__;
    }

    public function entityPostPersist(object $entity, LifecycleEventArgs $eventArgs): void
    {
        $entity->mark[] = __FUNCTION__;
    }

    public function entityPostRemove(object $entity, LifecycleEventArgs $eventArgs): void
    {
        $entity->mark[] = __FUNCTION__;
    }

    public function entityPostUpdate(object $entity, LifecycleEventArgs $eventArgs): void
    {
        $entity->mark[] = __FUNCTION__;
    }

    public function entityPrePersist(object $entity, LifecycleEventArgs $eventArgs): void
    {
        $entity->mark[] = __FUNCTION__;
    }

    public function entityPreRemove(object $entity, LifecycleEventArgs $eventArgs): void
    {
        $entity->mark[] = __FUNCTION__;
    }

    public function entityPreUpdate(object $entity, LifecycleEventArgs $eventArgs): void
    {
        $entity->mark[] = __FUNCTION__;
    }

    public function postLoad(LifecycleEventArgs $eventArgs): void
    {
        $entity = $eventArgs->getEntity();
        $entity->mark[] = __FUNCTION__;
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        $entity->mark[] = __FUNCTION__;
    }

    public function postRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        $entity->mark[] = __FUNCTION__;
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        $entity->mark[] = __FUNCTION__;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        $entity->mark[] = __FUNCTION__;
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        $entity->mark[] = __FUNCTION__;
    }

    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        $entity->mark[] = __FUNCTION__;
    }
}
