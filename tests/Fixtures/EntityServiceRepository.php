<?php


namespace MiCore\DoctrineBundle\Tests\Fixtures;


use Doctrine\Persistence\ManagerRegistry;
use MiCore\DoctrineBundle\Repository\ServiceEntityRepository;
use MiCore\DoctrineBundle\Tests\Fixtures\Entity\Entity;

class EntityServiceRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entity::class);
    }

}
