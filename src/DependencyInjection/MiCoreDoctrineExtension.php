<?php


namespace MiCore\DoctrineBundle\DependencyInjection;

use Doctrine\Common\Annotations\AnnotationReader;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityEventListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PostLoadListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PostPersistListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PostRemoveListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PostUpdateListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PrePersistListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PreRemoveListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\PreUpdateListenerInterface;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\FilterAnnotationsReader;
use MiCore\DoctrineBundle\Repository\HasFilterAnnotationReaderInterface;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;

class MiCoreDoctrineExtension extends Extension
{

    const ENTITY_EVENT_LISTENER_TAG_NAME = 'micore.doctrine.entity_event_listener';

    public function load(array $configs, ContainerBuilder $container)
    {
        $container->autowire(AnnotationReader::class);
        $container
            ->registerForAutoconfiguration(PrePersistListenerInterface::class)
            ->addTag('doctrine.event_listener', [
                'event' => Events::prePersist
            ]);
        $container
            ->registerForAutoconfiguration(PreUpdateListenerInterface::class)
            ->addTag('doctrine.event_listener', [
                'event' => Events::preUpdate
            ]);
        $container
            ->registerForAutoconfiguration(PreRemoveListenerInterface::class)
            ->addTag('doctrine.event_listener', [
                'event' => Events::preRemove
            ]);
        $container
            ->registerForAutoconfiguration(PostPersistListenerInterface::class)
            ->addTag('doctrine.event_listener', [
                'event' => Events::postPersist
            ]);
        $container
            ->registerForAutoconfiguration(PostUpdateListenerInterface::class)
            ->addTag('doctrine.event_listener', [
                'event' => Events::postUpdate
            ]);
        $container
            ->registerForAutoconfiguration(PostRemoveListenerInterface::class)
            ->addTag('doctrine.event_listener', [
                'event' => Events::postRemove
            ]);
        $container
            ->registerForAutoconfiguration(PostLoadListenerInterface::class)
            ->addTag('doctrine.event_listener', [
                'event' => Events::postLoad
            ]);


        $container
            ->registerForAutoconfiguration(EntityEventListenerInterface::class)
            ->addTag(self::ENTITY_EVENT_LISTENER_TAG_NAME);


        $container
            ->autowire(FilterAnnotationsReader::class);

        $container
            ->registerForAutoconfiguration(HasFilterAnnotationReaderInterface::class)
            ->addMethodCall('setFilterAnnotationReader', [new Reference(FilterAnnotationsReader::class)]);

        if (isset($_ENV['APP_ENV']) && 'test' == $_ENV['APP_ENV']){

            foreach ($container->getDefinitions() as $id=>$definition){
                $definition->setPublic(true);
            }
        }
    }
}
