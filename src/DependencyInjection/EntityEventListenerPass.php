<?php


namespace MiCore\DoctrineBundle\DependencyInjection;


use Doctrine\ORM\Events;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPostLoadListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPostPersistListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPostRemoveListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPostSaveListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPostUpdateListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPrePersistListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPreRemoveListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPreSaveListenerInterface;
use MiCore\DoctrineBundle\EventListener\Interfaces\EntityPreUpdateListenerInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class EntityEventListenerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {

         $entityEventListenerIds = $container->findTaggedServiceIds(MiCoreDoctrineExtension::ENTITY_EVENT_LISTENER_TAG_NAME);

         $methodRules = [
             EntityPrePersistListenerInterface::class => ['entityPrePersist', Events::prePersist],
             EntityPreUpdateListenerInterface::class => ['entityPreUpdate', Events::preUpdate],
             EntityPreRemoveListenerInterface::class => ['entityPreRemove', Events::preRemove],
             EntityPostPersistListenerInterface::class => ['entityPostPersist', Events::postPersist],
             EntityPostUpdateListenerInterface::class => ['entityPostUpdate', Events::postUpdate],
             EntityPostRemoveListenerInterface::class => ['entityPostRemove', Events::postRemove],
             EntityPostLoadListenerInterface::class => ['entityPostLoad', Events::postLoad],

             EntityPostSaveListenerInterface::class => ['entityPostSave', [Events::postPersist, Events::postUpdate]],
             EntityPreSaveListenerInterface::class => ['entityPreSave', [Events::prePersist, Events::preUpdate]],
         ];

         foreach ($entityEventListenerIds as $id => $tags){
             $definition = $container->getDefinition($id);
             $class = $definition->getClass();
             $entityClass = $class::entityClass();
             foreach ($methodRules as $subClass => [$method, $event]){
                 if (is_subclass_of($class, $subClass)){

                     $events = is_array($event) ? $event : [$event];
                     foreach ($events as $e) {
                         $definition->addTag('doctrine.orm.entity_listener', [
                             'event' => $e,
                             'method' => $method,
                             'lazy' => true,
                             'entity' => $entityClass
                         ]);

                     }
                 }
             }
         }
    }
}
