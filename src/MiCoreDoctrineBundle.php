<?php


namespace MiCore\DoctrineBundle;

use MiCore\DoctrineBundle\DependencyInjection\EntityEventListenerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MiCoreDoctrineBundle extends Bundle
{

    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new EntityEventListenerPass());
    }
}
