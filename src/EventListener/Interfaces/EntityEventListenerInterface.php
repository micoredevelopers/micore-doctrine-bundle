<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;


interface EntityEventListenerInterface
{

    /**
     * @return string
     */
    public static function entityClass(): string;

}
