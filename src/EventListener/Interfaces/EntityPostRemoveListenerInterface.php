<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface EntityPostRemoveListenerInterface extends EntityEventListenerInterface
{

    /**
     * @param object $entity
     * @param LifecycleEventArgs $eventArgs
     */
    public function entityPostRemove(object $entity, LifecycleEventArgs $eventArgs): void;

}
