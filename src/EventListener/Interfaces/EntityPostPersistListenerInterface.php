<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface EntityPostPersistListenerInterface extends EntityEventListenerInterface
{

    /**
     * @param object $entity
     * @param LifecycleEventArgs $eventArgs
     */
    public function entityPostPersist(object $entity, LifecycleEventArgs $eventArgs): void;

}
