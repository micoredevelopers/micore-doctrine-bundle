<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface PostLoadListenerInterface
{
    /**
     * @param LifecycleEventArgs $eventArgs
     * @return mixed
     */
    public function postLoad(LifecycleEventArgs $eventArgs): void;
}
