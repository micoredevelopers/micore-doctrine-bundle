<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;

use Doctrine\ORM\Event\LifecycleEventArgs;

interface PreUpdateListenerInterface
{

    /**
     * @param LifecycleEventArgs $args
     * @return mixed
     */
    public function preUpdate(LifecycleEventArgs $args): void;

}
