<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface PrePersistListenerInterface
{
    /**
     * @param LifecycleEventArgs $args
     * @return mixed
     */
    public function prePersist(LifecycleEventArgs $args): void;

}
