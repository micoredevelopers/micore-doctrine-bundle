<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface EntityPreRemoveListenerInterface extends EntityEventListenerInterface
{

    /**
     * @param object $entity
     * @param LifecycleEventArgs $eventArgs
     */
    public function entityPreRemove(object $entity, LifecycleEventArgs $eventArgs): void;

}
