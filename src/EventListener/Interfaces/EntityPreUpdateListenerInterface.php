<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface EntityPreUpdateListenerInterface extends EntityEventListenerInterface
{

    /**
     * @param object $entity
     * @param LifecycleEventArgs $eventArgs
     */
    public function entityPreUpdate(object $entity, LifecycleEventArgs $eventArgs): void;

}
