<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface PostRemoveListenerInterface
{

    /**
     * @param LifecycleEventArgs $args
     * @return mixed
     */
    public function postRemove(LifecycleEventArgs $args): void;

}

