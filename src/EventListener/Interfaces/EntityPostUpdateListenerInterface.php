<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface EntityPostUpdateListenerInterface extends EntityEventListenerInterface
{

    /**
     * @param object $entity
     * @param LifecycleEventArgs $eventArgs
     */
    public function entityPostUpdate(object $entity, LifecycleEventArgs $eventArgs): void;

}
