<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface PreRemoveListenerInterface
{

    /**
     * @param LifecycleEventArgs $args
     * @return mixed
     */
    public function preRemove(LifecycleEventArgs $args): void;
}
