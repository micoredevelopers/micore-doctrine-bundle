<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;

use Doctrine\ORM\Event\LifecycleEventArgs;

interface PostUpdateListenerInterface
{

    /**
     * @param LifecycleEventArgs $args
     * @return mixed
     */
    public function postUpdate(LifecycleEventArgs $args): void;

}
