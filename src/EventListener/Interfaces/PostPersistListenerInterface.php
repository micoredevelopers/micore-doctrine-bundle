<?php


namespace MiCore\DoctrineBundle\EventListener\Interfaces;



use Doctrine\ORM\Event\LifecycleEventArgs;

interface PostPersistListenerInterface
{

    /**
     * @param LifecycleEventArgs $args
     * @return mixed
     */
    public function postPersist(LifecycleEventArgs $args): void;

}
