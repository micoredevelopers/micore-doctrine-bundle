<?php


namespace MiCore\DoctrineBundle\Repository;


use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\FilterAnnotationsReader;

interface HasFilterAnnotationReaderInterface
{

    /**
     * @param FilterAnnotationsReader $filterAnnotationsReader
     */
    public function setFilterAnnotationReader(FilterAnnotationsReader $filterAnnotationsReader): void;

}
