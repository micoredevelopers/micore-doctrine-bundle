<?php


namespace MiCore\DoctrineBundle\Repository\Pagination;


use Doctrine\ORM\QueryBuilder;

interface PaginationInterface
{
    /**
     * @return int
     */
    public function getPage(): int;

    /**
     * @return int
     */
    public function getLimit(): int;

    /**
     * @return int|null
     */
    public function getQ(): ?int;

    /**
     * @param QueryBuilder $queryBuilder
     * @return mixed
     */
    public function handleQueryBuilder(QueryBuilder $queryBuilder);
}
