<?php


namespace MiCore\DoctrineBundle\Repository\Pagination;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

class Pagination implements PaginationInterface
{

    const DEFAULT_LIMIT = 10;

    private $page;

    private $limit;

    private $q;

    public function __construct(int $page = 1, int $limit = self::DEFAULT_LIMIT)
    {
        if (0 >= $page){
            $page = 1;
        }
        $this->page = $page;
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit(int $limit): self
    {
        return $this;
    }

    /**
      * @return int|null
     */
    public function getQ(): ?int
    {
        return $this->q;
    }

    /**
     * @return int
     */
    public function getCountPages(): int
    {

        $count = $this->q;
        $limit = $this->limit;
        return ceil($count/$limit);
    }

    /**
     * @return bool
     */
    public function nextExist(): bool
    {
        return $this->getCountPages() > $this->page;
    }

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function handleQueryBuilder(QueryBuilder $queryBuilder): void
    {
        $paginator = new Paginator($queryBuilder);
        $paginator->setUseOutputWalkers(false);
        $this->q = $paginator->count();
        $queryBuilder
            ->setMaxResults($this->limit)
            ->setFirstResult($this->limit * ($this->page - 1));
    }

}
