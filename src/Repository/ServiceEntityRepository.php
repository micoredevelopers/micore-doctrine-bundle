<?php


namespace MiCore\DoctrineBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\ORMException;
use MiCore\DoctrineBundle\Repository\Pagination\PaginationInterface;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\FilterAnnotationsReader;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository as ServiceEntityRepositoryOriginal;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

abstract class ServiceEntityRepository extends ServiceEntityRepositoryOriginal implements HasFilterAnnotationReaderInterface
{

    private $entityClass;

    /**
     * @var Collection
     */
    private $collectionAll;

    /**
     * @var FilterAnnotationsReader
     */
    private $filterAnnotationsReader;

    public function __construct(ManagerRegistry $registry, string $entityClass)
    {
        parent::__construct($registry, $entityClass);
        $this->entityClass = $entityClass;
    }

    public function createQueryBuilder($alias, $indexBy = null): QueryBuilderDecorator
    {
        $qb = new QueryBuilderDecorator($this->_em);
        $qb->select($alias)
            ->from($this->_entityName, $alias, $indexBy);
        return $qb;
    }


    /**
     * @param PaginationInterface|null $pagination
     * @param mixed|null $filter
     * @return EntityCollection
     * @throws \Exception
     */
    public function findElements(?PaginationInterface $pagination = null, $filter = null): Collection
    {

        $queryBuilder = $this->createQueryBuilderByFilter('e', $filter);
        if (null !== $pagination) {
            $pagination->handleQueryBuilder($queryBuilder);
        }
        $elements = $queryBuilder->getQuery()->getResult();
        return $this->createCollection($elements);
    }

    /**
     * @return Collection
     */
    public function findCollectionAll(): Collection
    {
        return $this->createCollection(parent::findAll());
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return Collection|array
     */
    public function findCollectionBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): Collection
    {
        $elements = parent::findBy($criteria, $orderBy, $limit, $offset);
        return $this->createCollection($elements);
    }

    /**
     * @param string $alias
     * @param mixed|null $filter
     * @return QueryBuilderDecorator
     * @throws \Exception
     */
    public function createQueryBuilderByFilter(string $alias, $filter = null): QueryBuilderDecorator
    {
        $qb = $this->createQueryBuilder($alias);
        if (null === $filter) {
            return $qb;
        }

        $criteria = $this->filterAnnotationsReader->read($filter);
        $qb->addCriteria($criteria);
        return $qb;
    }

    /**
     * @param mixed|null $filterData
     * @return int
     * @throws \Exception
     */
    public function getCountByFilter($filterData = null): int
    {
        $qb = $this->createQueryBuilderByFilter($filterData);
        $paginator = new Paginator($qb);
        $paginator->setUseOutputWalkers(false);
        return $paginator->count();
    }

    /**
     * @param array $elements
     * @return Collection
     */
    public function createCollection(array $elements = []): Collection
    {
        return new EntityCollection($elements);
    }

    public function getCollectionAll(): Collection
    {
        if (null === $this->collectionAll){
            $this->collectionAll = $this->findCollectionAll();
        }
        return $this->collectionAll;
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     */
    public function persist($entity): void
    {
        $em = $this->getEntityManager();
        if (is_array($entity)) {
            foreach ($entity as $item) {
                if ($em->contains($item)) {
                    continue;
                }
                $em->persist($item);
            }
        } else {
            $em->persist($entity);
        }
    }

    /**
     * @param Collection $entityCollection
     * @return Collection
     * @throws ORMException
     */
    public function setAll(Collection $entityCollection): Collection
    {
        $removedCollection = new ArrayCollection();
        foreach ($this->findAll() as $certificate) {
            if (!$entityCollection->contains($certificate)) {
                $this->_em->remove($certificate);
                $removedCollection->add($certificate);
            }
        }
        foreach ($entityCollection as $certificate) {
            $this->_em->persist($certificate);
        }
        return $removedCollection;
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove($entity): void
    {
        $em = $this->getEntityManager();
        if (is_array($entity)) {
            foreach ($entity as $item) {
               $em->remove($item);
            }
        } else {
            $em->remove($entity);
        }
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }

    /**
     * @param FilterAnnotationsReader $filterAnnotationsReader
     */
    public function setFilterAnnotationReader(FilterAnnotationsReader $filterAnnotationsReader): void
    {
        $this->filterAnnotationsReader = $filterAnnotationsReader;
    }
}
