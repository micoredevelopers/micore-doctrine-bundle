<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations;


use Throwable;

class AnnotationDoesNotExistException extends \Exception
{

    /**
     * AnnotationDoesNotExistException constructor.
     * @param $annotation
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($annotation, $code = 0, Throwable $previous = null)
    {
        parent::__construct('Annotation '.get_class($annotation).' does not exist', $code, $previous);
    }

}
