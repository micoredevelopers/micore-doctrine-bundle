<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations;


use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\AndX;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\Comp;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\DateComp;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\DateEq;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\DefaultComp;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\Order;
use MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison\OrX;
use MiCore\DoctrineBundle\Repository\QueryBuilderDecorator;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;

class FilterAnnotationsReader
{

    /**
     * @var AnnotationReader
     */
    private $annotationReader;

    /**
     * @var PropertyAccessor
     */
    private $propertyAccessor;


    /**
     * @return AnnotationReader
     */
    private function annotationReader(): AnnotationReader
    {
        if (null === $this->annotationReader) {
            $this->annotationReader = new AnnotationReader();
        }
        return $this->annotationReader;
    }

    /**
     * @return PropertyAccessor
     */
    private function propertyAccessor(): PropertyAccessor
    {
        if (null === $this->propertyAccessor) {
            $this->propertyAccessor = new PropertyAccessor();
        }
        return $this->propertyAccessor;
    }


    /**
     * @param $filter
     * @return Criteria
     * @throws AnnotationDoesNotExistException
     * @throws InvalidValueTypeException
     * @throws \ReflectionException
     */
    public function read($filter): Criteria
    {
        $annotationReader = $this->annotationReader();

        $reflect = new \ReflectionClass(get_class($filter));
        $props   = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED | \ReflectionProperty::IS_PRIVATE);
        $properties = array_map(function(\ReflectionProperty $property){
            return $property->getName();
        }, $props);

//        if (!$properties){
//            throw new \InvalidArgumentException('Object has no attributes available');
//        }

        $criteria = Criteria::create();

        foreach ($properties as $property) {

            if (!$this->propertyAccessor()->isReadable($filter, $property)){
                continue;
            }

            $reflectionProperty = new \ReflectionProperty(get_class($filter), $property);
            $annotation = $annotationReader->getPropertyAnnotations($reflectionProperty);
            if ([] === $annotation) {
                continue;
            }
            $value = $this->propertyAccessor()->getValue($filter, $property);

            if ($value instanceof Collection){
                $value = $value->count() ? $value->toArray() : null;
            }

            if (null === $value) {
                continue;
            }

            $this->loadCriteria($value, $annotation, $criteria);
        }
        return $criteria;
    }

    /**
     * @param $value
     * @param array $annotation
     * @param Criteria $criteria
     * @throws AnnotationDoesNotExistException
     * @throws InvalidValueTypeException
     */
    private function loadCriteria($value, array $annotation, Criteria $criteria)
    {

        $comparisonsOr = [];
        $comparisonsAnd = [];

        $comp = 'or';
        foreach ($annotation as $a) {

            if ($a instanceof OrX){
                $comp = 'or';
                continue;
            } elseif ($a instanceof AndX){
                $comp = 'and';
                continue;
            }

            if (null === $a->value) {
                $val = $value;
            } else {
                $val = $a->value;
            }

            if ($a instanceof Order){
                 $criteria->orderBy([$a->field => $val]);
                 continue;
            }

            if ('and' === $comp){
                $comparisonsAnd[] = $this->createComp($a, $val);
            } else {
                $comparisonsOr[] = $this->createComp($a, $val);
            }

        }
        if ($comparisonsOr) {

            $criteria->andWhere(Criteria::expr()->orX(...$comparisonsOr));

        }
        if ($comparisonsAnd) {
            $criteria->andWhere(Criteria::expr()->andX(...$comparisonsAnd));
        }
    }

    /**
     * @param DefaultComp $annotation
     * @param $value
     * @return \Doctrine\Common\Collections\Expr\CompositeExpression|mixed
     * @throws AnnotationDoesNotExistException
     * @throws InvalidValueTypeException
     */
    private function createComp(DefaultComp $annotation, $value)
    {

        if ($annotation instanceof DateComp) {
            $this->mustBeInstanceOf(\DateTimeInterface::class, $value);
            $val = QueryBuilderDecorator::dateAccuracy($value, $annotation->accuracy, $annotation->moreLess === 'more');
            return call_user_func_array([Criteria::expr(), $annotation->exprMethod], [$annotation->field, $val]);
        }

        if ($annotation instanceof DateEq) {
            $this->mustBeInstanceOf(\DateTimeInterface::class, $value);
            $from = QueryBuilderDecorator::dateAccuracy($value, $annotation->accuracy, true);
            $to = QueryBuilderDecorator::dateAccuracy($value, $annotation->accuracy, false);

            return Criteria::expr()->andX(
                Criteria::expr()->gte($annotation->field, $from),
                Criteria::expr()->lte($annotation->field, $to)
            );
        }

        if ($annotation instanceof Comp) {
            return call_user_func_array([Criteria::expr(), $annotation->exprMethod], [$annotation->field, $value]);
        }

        throw new AnnotationDoesNotExistException($annotation);

    }

    /**
     * @param $instance
     * @param $value
     * @throws InvalidValueTypeException
     */
    private function mustBeInstanceOf($instance, $value)
    {
        if (!($value instanceof $instance)) {
            throw new InvalidValueTypeException($instance);
        }
    }

}
