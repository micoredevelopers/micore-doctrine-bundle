<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations;


use Throwable;

class InvalidValueTypeException extends \Exception
{
    public function __construct(string $type = null, $message = 'Value type must be ', $code = 0, Throwable $previous = null)
    {
        if (null !== $type && null !== $message){

        }
        parent::__construct($message. ' '.$type, $code, $previous);
    }
}
