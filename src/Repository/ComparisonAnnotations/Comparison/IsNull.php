<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;


/**
 * Class IsNull
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class IsNull extends Comp
{

    public $exprMethod = 'isNull';

}
