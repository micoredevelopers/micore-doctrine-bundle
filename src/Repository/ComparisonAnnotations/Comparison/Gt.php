<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;

/**
 * Class Gt
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class Gt extends Comp
{
    /**
     * @var string
     */
    public $exprMethod = 'gt';
}
