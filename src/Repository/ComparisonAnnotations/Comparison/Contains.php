<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;

/**
 * Class Contains
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class Contains extends Comp
{

    /**
     * @var string
     */
    public $exprMethod = 'contains';

}
