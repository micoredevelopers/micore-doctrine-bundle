<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;

/**
 * Class DateEq
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class DateEq extends DefaultComp
{

    /**
     * d,m,Y
     *
     * @var string
     */
    public $accuracy = 'd';

}
