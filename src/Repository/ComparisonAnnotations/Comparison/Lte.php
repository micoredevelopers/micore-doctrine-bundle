<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;

/**
 * Class Lte
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class Lte extends Comp
{

    /**
     * @var string
     */
    public $exprMethod = 'lte';

}
