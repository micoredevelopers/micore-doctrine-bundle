<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;


/**
 * Class Eq
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class Eq extends Comp
{

    public $exprMethod = 'eq';

}
