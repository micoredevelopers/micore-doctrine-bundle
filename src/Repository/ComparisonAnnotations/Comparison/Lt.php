<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;


/**
 * Class Lt
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class Lt extends Comp
{
    /**
     * @var string
     */
    public $exprMethod = 'lt';
}
