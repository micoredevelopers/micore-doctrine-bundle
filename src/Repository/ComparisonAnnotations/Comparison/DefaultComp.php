<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;


class DefaultComp
{

    /**
     * @var string
     */
    public $field;

    /**
     * @var mixed
     */
    public $value;

    public function __construct(array $data = [])
    {
        if (isset($data['value'])){
            $this->field = $data['value'];
            unset($data['value']);
        }

        foreach ($data as $property=>$value){
            $this->$property = $value;
        }

    }

}
