<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;


/**
 * Class MemberOf
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class MemberOf extends Comp
{

    /**
     * @var string
     */
    public $exprMethod = 'MemberOf';

}
