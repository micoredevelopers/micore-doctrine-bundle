<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;

/**
 * Class NotIn
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class NotIn extends Comp
{

    /**
     * @var string
     */
    public $exprMethod = 'notIn';

}
