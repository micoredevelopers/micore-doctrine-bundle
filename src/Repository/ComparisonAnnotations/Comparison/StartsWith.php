<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;


/**
 * Class StartsWith
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class StartsWith extends Comp
{

    /**
     * @var string
     */
    public $exprMethod = 'startsWith';

}
