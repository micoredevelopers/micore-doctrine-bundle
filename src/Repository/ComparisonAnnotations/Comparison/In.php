<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;

/**
 * Class In
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class In extends Comp
{

    /**
     * @var string
     */
    public $exprMethod = 'in';

}
