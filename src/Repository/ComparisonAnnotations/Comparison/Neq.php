<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;

/**
 * Class Neq
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class Neq extends Comp
{
    /**
     * @var string
     */
    public $exprMethod = 'neq';
}
