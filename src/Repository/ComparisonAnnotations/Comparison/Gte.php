<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;


/**
 * Class Gte
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class Gte extends Comp
{

    /**
     * @var string
     */
    public $exprMethod = 'gte';

}
