<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;

/**
 * Class Comp
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 *
 * @Annotation
 */
class Comp extends DefaultComp
{

    /**
     * @var string
     */
    public $exprMethod;
}
