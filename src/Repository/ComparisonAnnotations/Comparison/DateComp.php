<?php


namespace MiCore\DoctrineBundle\Repository\ComparisonAnnotations\Comparison;

/**
 * Class DateComp
 * @package Advance\DoctrineBundle\Repository\FilterAnnotations
 *
 * @Annotation
 */
class DateComp extends Comp
{

    /**
     * d,m,Y
     *
     * @var string
     */
    public $accuracy = 'd';

    /**
     * @var string
     */
    public $moreLess;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (null === $this->exprMethod){
            $this->exprMethod = 'eq';
        }
    }
}
