<?php


namespace MiCore\DoctrineBundle\Repository;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\QueryBuilder;

class QueryBuilderDecorator extends QueryBuilder
{

    const DAY = 'd';
    const MONTH = 'm';
    const YEAR = 'Y';

    private $parameterQ = 1;

    /**
     * @param string $fieldName
     * @param \DateTimeInterface $dateTime
     * @param string $accuracy
     * @return $this
     * @throws \Exception
     */
    public function dateEqual(string $fieldName, \DateTimeInterface $dateTime, $accuracy = self::DAY): self
    {
        return $this
            ->dateFrom($fieldName, $dateTime, $accuracy)
            ->dateTo($fieldName, $dateTime, $accuracy);
    }

    /**
     * @param string $fieldName
     * @param \DateTimeInterface $dateTime
     * @param string $accuracy
     * @return $this
     * @throws \Exception
     */
    public function dateFrom(string $fieldName, \DateTimeInterface $dateTime, $accuracy = self::DAY): self
    {
        $from = self::dateAccuracy($dateTime, $accuracy, true);
        $parameterNameDataFrom = $this->getParameterName();
        $this
            ->andWhere(sprintf('%s >= :%s', $fieldName, $parameterNameDataFrom))
            ->setParameter($parameterNameDataFrom, $from);
        return $this;
    }

    /**
     * @param string $fieldName
     * @param \DateTimeInterface $dateTime
     * @param string $accuracy
     * @return $this
     * @throws \Exception
     */
    public function dateTo(string $fieldName, \DateTimeInterface $dateTime, $accuracy = self::DAY): self
    {
        $to = self::dateAccuracy($dateTime, $accuracy, false);
        $parameterNameDataTo = $this->getParameterName();
        $this
            ->andWhere(sprintf('%s <= :%s', $fieldName, $parameterNameDataTo))
            ->setParameter($parameterNameDataTo, $to);
        return $this;
    }

    /**
     * @param \DateTimeInterface $dateTime
     * @param $accuracy
     * @param bool $from
     * @return \DateTimeInterface
     * @throws \Exception
     */
    public static function dateAccuracy(\DateTimeInterface $dateTime, $accuracy, bool $from): \DateTimeInterface
    {
        $time = true === $from ? '00:00:00' : '23:59:59';
        $year = $dateTime->format('Y');
        if (self::YEAR === $accuracy){
            $month = true === $from ? '01' : '12';
        } else {
            $month = $dateTime->format('m');
        }

        if (self::MONTH === $accuracy || self::YEAR == $accuracy){
            $day = true === $from ? '01' : cal_days_in_month(CAL_GREGORIAN, $month, $year);
        } else {
            $day = $dateTime->format('d');
        }

        return new \DateTime($year.'-'.$month.'-'.$day.' '.$time);
    }

    /**
     * @param $join
     * @param $alias
     * @param null $conditionType
     * @param null $condition
     * @param null $indexBy
     * @return $this
     */
    public function leftJoinSafe($join, $alias, $conditionType = null, $condition = null, $indexBy = null): self
    {

        $joinDqlParts = $this->getDQLParts()['join'];
        foreach ($joinDqlParts as $joins) {
            foreach ($joins as $join_) {
                if ($join_->getAlias() === $alias) {
                    return $this;
                }
            }
        }

        $this->leftJoin($join, $alias, $conditionType, $condition, $indexBy);
        return $this;
    }

    /**
     * @param string $field
     * @return string
     */
    public function leftJoinByField(string $field): string
    {
        $expField = explode('.', $field);
        if (count($expField) > 2) {

            $field_ = $expField[0] . '.' . $expField[1];
            $alias = str_replace('.', '_', $field_);
            $this->leftJoinSafe($field_, $alias);

            foreach (array_slice($expField, 2, -1) as $f) {
                $field_ = $alias.'.' . $f;

                $alias = str_replace('.', '_', $field_);
                $this->leftJoinSafe($field_, $alias);
            }
            return $alias.'.'.array_pop($expField);
        }
        return $field;
    }

    /**
     * @return string
     */
    private function getParameterName(): string
    {
        $this->parameterQ++;
        return 'param_' . $this->parameterQ;
    }


    /**
     * Adds criteria to the query.
     *
     * Adds where expressions with AND operator.
     * Adds orderings.
     * Overrides firstResult and maxResults if they're set.
     *
     * @param Criteria $criteria
     *
     * @return self
     *
     * @throws QueryException
     */
    public function addCriteria(Criteria $criteria)
    {
        $allAliases = $this->getAllAliases();
        if ( ! isset($allAliases[0])) {
            throw new QueryException('No aliases are set before invoking addCriteria().');
        }

        $visitor = new QueryExpressionVisitorDecorator($this->getAllAliases(), $this);

        if ($whereExpression = $criteria->getWhereExpression()) {
            $this->andWhere($visitor->dispatch($whereExpression));
            /**
             * @var Parameter $parameter
             */
            foreach ($visitor->getParameters() as $parameter) {
                $this->setParameter($parameter->getName(), $parameter->getValue());
            }
        }

        if ($criteria->getOrderings()) {
            foreach ($criteria->getOrderings() as $sort => $order) {

                $hasValidAlias = false;
                foreach($allAliases as $alias) {
                    if(strpos($sort . '.', $alias . '.') === 0) {
                        $hasValidAlias = true;
                        break;
                    }
                }

                if(!$hasValidAlias) {
                    $sort = $allAliases[0] . '.' . $sort;
                }

                $this->addOrderBy($sort, $order);
            }
        }

        // Overwrite limits only if they was set in criteria
        if (($firstResult = $criteria->getFirstResult()) !== null) {
            $this->setFirstResult($firstResult);
        }
        if (($maxResults = $criteria->getMaxResults()) !== null) {
            $this->setMaxResults($maxResults);
        }

        return $this;
    }


}
